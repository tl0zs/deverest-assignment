//
//  FlowLayout.swift
//  Deverest assignment
//
//  Created by Zsolt Magyi on 2/27/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import Foundation
import UIKit

class FlowLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
    }
    
    init(with frame: CGRect) {
        super.init()
        let gap: CGFloat = 20
        
        itemSize = CGSize(width: frame.width - gap * 2, height: frame.height - gap * 2)
        scrollDirection = .horizontal
        minimumInteritemSpacing = gap / 2
        minimumLineSpacing = gap / 2
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
