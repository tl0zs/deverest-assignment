//
//  MovieDetailVC.swift
//  Deverest assignment
//
//  Created by Zsolt Magyi on 2/27/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import UIKit

class MovieDetailVC: UIViewController {
    static let storyboardID = "MovieDetailVC"
    var movie: Movie!
    
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imdbLabel: UILabel!
    @IBOutlet weak var imdbRatingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var releaseLabel: UILabel!
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initLayout()
        addGestureToDismiss()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: Actions
    @IBAction func dismissVC(_ sender: Any) {
        popVC()
    }
}

//MARK: Helpers
extension MovieDetailVC {
    @objc fileprivate func popVC() {
        navigationController?.popViewController(animated: true)
    }
    
    fileprivate func initLayout() {
        dismissButton.layer.shadowOpacity = 0.3
        
        coverImageView.image = movie.getCover()
        titleLabel.text = movie.title
        releaseLabel.text = "| \(movie.getReleaseYear()!)"
        
        imdbLabel.layer.borderColor = UIColor.lightGray.cgColor
        imdbLabel.layer.borderWidth = 1
        imdbRatingLabel.text = "\(movie.vote_average)"
        
        descriptionLabel.text = movie.overview
        descriptionLabel.numberOfLines = 0
        descriptionLabel.sizeToFit()
    }
    
    fileprivate func addGestureToDismiss() {
        let sweep = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(popVC))
        sweep.edges = .left
        view.addGestureRecognizer(sweep)
    }
}
