//
//  MoviesMasterVC.swift
//  Deverest assignment
//
//  Created by Zsolt Magyi on 2/26/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import UIKit

class MoviesMasterVC: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var reloadListButton: UIButton!
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    
    fileprivate var movies: [Movie]?
    fileprivate var searchedMovies: [Movie]?
    fileprivate var shouldShowSearchResults = false
    fileprivate var pageCount = 1
    
    //MARK: Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initLayout()
        initCollection()
        downloadNewMovies()
        initSearchBar()
    }
    
    //MARK: Callables
    @IBAction func reloadList(_ sender: Any) {
        movies?.removeAll()
        movies = nil
        pageCount = 1
        shouldShowSearchResults = false
        downloadNewMovies()
    }
}

//MARK: Helpers
extension MoviesMasterVC {
    fileprivate func initLayout() {
        reloadListButton.layer.borderColor = UIColor.black.cgColor
        reloadListButton.layer.borderWidth = 1
        reloadListButton.layer.cornerRadius = reloadListButton.frame.height / 2
    }
    
    fileprivate func initSearchBar() {
        searchBar.delegate = self
    }
}

//MARK: Searchbar
extension MoviesMasterVC: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text  {
            if text.count < 3 {
                showTooShortQuery()
            } else {
                shouldShowSearchResults = true
                performSearch(with: text)
            }
        }
        searchBar.resignFirstResponder()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        shouldShowSearchResults = false
        reloadMovies()
    }
    
    private func showTooShortQuery() {
        let alert = UIAlertController(title: "Error", message: "Too short query. Please enter at least 3 chars.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
}

//MARK: Network related
extension MoviesMasterVC {
    fileprivate func downloadNewMovies() {
        Networking.shared.getDiscoverableMovies(page: pageCount) { [weak self] (movieResult) in
            switch movieResult {
            case .success(let moviesDict):
                for movieNode in moviesDict {
                    if let movie = Movie.unwrapMovie(movieNode: movieNode) {
                        self?.addMovieToDb(movie)
                    }
                }
                
                DispatchQueue.main.async {
                    self?.reloadMovies()
                }
            case .failure(let networkError):
                print(networkError.localizedDescription)
            }
        }
    }
    
    fileprivate func performSearch(with text: String) {
        Networking.shared.searchForMovie(with: text) { [weak self] (result) in
            switch result {
            case .failure(let error):
                print("Error: \(error)")
            case .success(let movies):
                self?.removeAllSearchedMovies()
                
                for movieNode in movies {
                    if let movie = Movie.unwrapMovie(movieNode: movieNode) {
                        self?.addMovieToSearchedDb(movie)
                    }
                }
                
                DispatchQueue.main.async {
                    self?.reloadMovies()
                }
            }
        }
    }
    
    fileprivate func downloadCoverFor(_ indexPath: IndexPath) {
        if let movie = movies?[indexPath.row] {
            Networking.shared.downloadCoverImage(urlPath: movie.poster_path) { (imgData) in
                if imgData != nil {
                    movie.setCover(data: imgData)
                    DispatchQueue.main.async { [weak self] in
                        self?.moviesCollectionView.reloadItems(at: [indexPath])
                    }
                }
            }
        }
    }
    
    fileprivate func downloadMovieBudget(_ indexPath: IndexPath) {
        if let movie = movies?[indexPath.row] {
            Networking.shared.downloadBudgetForMovie(ID: movie.id) { (budget) in
                if budget != nil {
                    movie.budget = budget
                    DispatchQueue.main.async { [weak self] in
                        self?.moviesCollectionView.reloadItems(at: [indexPath])
                    }
                }
            }
        }
    }
    
    fileprivate func addMovieToDb(_ movie: Movie) {
        if movies == nil {
            movies = [Movie]()
        }
        movies?.append(movie)
    }
    
    fileprivate func addMovieToSearchedDb(_ movie: Movie) {
        if searchedMovies == nil {
            searchedMovies = [Movie]()
        }
        searchedMovies?.append(movie)
    }
    
    fileprivate func removeAllSearchedMovies() {
        searchedMovies?.removeAll()
        searchedMovies = nil
    }
}

//MARK: Collection view delegate
extension MoviesMasterVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let mvs = self.shouldShowSearchResults ? self.searchedMovies : self.movies
        return mvs != nil ? mvs!.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCell.cellId, for: indexPath) as! MovieCell
        let mvs = self.shouldShowSearchResults ? self.searchedMovies : self.movies
        guard let movie = mvs?[indexPath.row] else { return cell }
        
        cell.imdbScoreLabel.text = "\(movie.vote_average)"
        cell.titleLabel.text = movie.title + " (\(movie.getReleaseYear()!))"
        
        if let cover = movie.getCover() {
            cell.movieImageView.image = cover
        } else {
            downloadCoverFor(indexPath)
        }
        
        if let budget = movie.budget {
            cell.budgetLabel.text = "$" + budget.countMillions() + "M"
        } else {
            downloadMovieBudget(indexPath)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let mvs = self.shouldShowSearchResults ? self.searchedMovies : self.movies
        if let count = mvs?.count {
            if indexPath.row > count - 3 {
                pageCount += 1
                downloadNewMovies()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("selected cell: \(indexPath.row)")
        let mvs = self.shouldShowSearchResults ? self.searchedMovies : self.movies
        if let movie = mvs?[indexPath.row] {
            if let detailVC = storyboard?.instantiateViewController(withIdentifier: MovieDetailVC.storyboardID) as? MovieDetailVC {
                detailVC.movie = movie
                navigationController?.pushViewController(detailVC, animated: true)
            }
        }
    }
    
    fileprivate func initCollection() {
        moviesCollectionView.delegate = self
        moviesCollectionView.dataSource = self
        moviesCollectionView.collectionViewLayout = FlowLayout(with: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 120)))
    }
    
    fileprivate func reloadMovies() {
        moviesCollectionView.reloadData()
    }
}
