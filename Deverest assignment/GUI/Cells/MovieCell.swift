//
//  MovieCell.swift
//  Deverest assignment
//
//  Created by Zsolt Magyi on 2/26/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {
    static let cellId = "movieCellId"
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imdbLabel: UILabel!
    @IBOutlet weak var imdbScoreLabel: UILabel!
    @IBOutlet weak var budgetLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initLayout()
    }
}

extension MovieCell {
    fileprivate func initLayout() {
        imdbLabel.layer.borderWidth = 1
        imdbLabel.layer.borderColor = UIColor.lightGray.cgColor
        
        movieImageView.layer.cornerRadius = 20
        movieImageView.layer.masksToBounds = true
    }
}
