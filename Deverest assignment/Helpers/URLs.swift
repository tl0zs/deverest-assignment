//
//  URLs.swift
//  Deverest assignment
//
//  Created by Zsolt Magyi on 2/26/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import Foundation

struct URLs {
    static let baseURL = "https://api.themoviedb.org/3"
    
    static let discover = baseURL + "/discover/movie?"
    static let poster = "https://image.tmdb.org/t/p/w500"
    static let budget = baseURL + "/movie/"
    static let search = baseURL + "/search/movie?"
}
