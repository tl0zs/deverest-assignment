//
//  Networking.swift
//  Deverest assignment
//
//  Created by Zsolt Magyi on 2/26/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import Foundation

typealias MovieResult = Result<[[String:AnyObject]], NetworkError>

class Networking {
    static let shared = Networking()
    private init() {}
    
    func getDiscoverableMovies(page: Int, completion: @escaping ((MovieResult) -> Void)) {
        guard let request = createRequest(urlStr: URLs.discover, type: .GET, queryGroup: .discover(page: page)) else {
            completion(.failure(.requestError))
            return
        }
        
        createSession(request, completion: { (result) in
            switch result {
            case .success(let data):
                if let responseDict = JSONConverter.JSON2Dict(data: data), let results = responseDict["results"] as? [[String: AnyObject]] {
                    completion(.success(results))
                } else {
                    completion(.failure(.invalidJsonFomatError))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
    
    func searchForMovie(with text: String, completion: @escaping ((MovieResult) -> Void)) {
        guard let request = createRequest(urlStr: URLs.search, type: .GET, queryGroup: .search(query: text)) else {
            completion(.failure(.requestError))
            return
        }
        
        createSession(request, completion: { (result) in
            switch result {
            case .success(let data):
                if let responseDict = JSONConverter.JSON2Dict(data: data)?["results"] as? [[String: AnyObject]]{
                    completion(.success(responseDict))
                } else {
                    completion(.failure(.invalidJsonFomatError))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
    
    func downloadCoverImage(urlPath: String, completion: @escaping ((Data?) -> Void)) {
        guard let request = createRequest(urlStr: URLs.poster + urlPath, type: .GET, queryGroup: .coverImage) else {
            return
        }
        
        createSession(request, completion: { (result) in
            switch result {
            case .success(let data):
                completion(data)
            case .failure(let error):
                print("Couldn't find the image with url: \(error.localizedDescription)")
                completion(nil)
            }
        })
    }
    
     func downloadBudgetForMovie(ID: Int, completion: @escaping ((Int?) -> Void)) {
           guard let request = createRequest(urlStr: URLs.budget + "\(ID)", type: .GET, queryGroup: .budget) else {
               return
           }
           
        createSession(request, completion: { (result) in
            switch result {
            case .success(let data):
                if let budget = JSONConverter.JSON2Dict(data: data)?["budget"] as? Int {
                    completion(budget)
                } else {
                    print("Couldn't find the budget")
                    completion(nil)
                }
                
            case .failure(let error):
                print("Couldn't find budget \(error.localizedDescription)")
                completion(nil)
            }
        })
    }
}

//MARK: Helpers
extension Networking {
    fileprivate func createSession(_ request: URLRequest, completion: @escaping ((Result<Data, NetworkError>) -> Void)) {
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            if data == nil {
                completion(.failure(.noResponseError))
            } else {
                if let statusCode = self.getStatusCode(response: response) {
                    if (200..<300) ~= statusCode {
                        completion(.success(data!))
                    } else {
                        completion(.failure(.invalidSessionError))
                    }
                } else {
                    completion(.failure(.invalidSessionError))
                }
            }
            
            
        })
        if task.state == .running {
            task.cancel()
        }
        task.resume()
        session.finishTasksAndInvalidate()
    }
    
    
    fileprivate func createRequest(urlStr: String,
                                   type: HTTPTYPE,
                                   body: Dictionary<String, AnyObject>? = nil,
                                   headers: Dictionary<String, String>? = nil,
                                   queryGroup: QueryGroup,
                                   bodyData: Data? = nil) -> URLRequest? {
        
        var urlComps = URLComponents(string: urlStr)!
        addQueryToURLComponents(&urlComps, queryItems: queryGroup.getQueryItems())

        let request = NSMutableURLRequest(url: urlComps.url!)
        request.httpMethod = type.rawValue

        if headers != nil {
            addHeadersToRequest(request, headers: headers!)
        }

        if body != nil {
            addBodyToRequest(request, body: body!)
        } else if bodyData != nil {
            request.httpBody = bodyData!
        }

        return request as URLRequest
    }
    
    fileprivate func getStatusCode(response: URLResponse?) -> Int? {
        if let httpResponse = response as? HTTPURLResponse {
            return httpResponse.statusCode
        }
        return nil
    }
    
    private func addQueryToURLComponents(_ component: inout URLComponents, queryItems: [MovieQueryItem]) {
        var items = [URLQueryItem]()
        for item in queryItems {
            items.append(URLQueryItem(name: item.key, value: item.value))
        }
        
        component.queryItems = items
    }
    
    private func addHeadersToRequest(_ request: NSMutableURLRequest, headers: Dictionary<String, String>) {
        for (key, value) in headers {
            request.addValue(value, forHTTPHeaderField: key)
        }
    }
    
    private func addBodyToRequest(_ request: NSMutableURLRequest, body: Dictionary<String, AnyObject>) {
        //FIXME: Implement me
//        let data = JSONConverter.dict2JSON(dict: body)
//        request.httpBody = data
    }
}
