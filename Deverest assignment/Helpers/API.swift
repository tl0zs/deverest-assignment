//
//  API.swift
//  Deverest assignment
//
//  Created by Zsolt Magyi on 2/26/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import Foundation

enum API: String {
    case key = "api_key"
    case value = "555dd34b51d2f5b7f9fdb39e04986933"
}
