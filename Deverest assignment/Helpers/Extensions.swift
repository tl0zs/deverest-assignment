//
//  Extensions.swift
//  Deverest assignment
//
//  Created by Zsolt Magyi on 2/27/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import Foundation


extension Int {
    func countMillions() -> String {
        if self == 0 {
            return "0"
        }
        
        let millions = round(Double(self) / Double(1000000))
        return String(format: "%.2f", millions)
    }
}
