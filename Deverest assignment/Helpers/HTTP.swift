//
//  HTTP.swift
//  Deverest assignment
//
//  Created by Zsolt Magyi on 2/26/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import Foundation

enum HTTPTYPE: String {
    case GET
    case POST
    case PUT
    case DELETE
}

struct HTTPHeader {
    struct Keys {
        static let authorization = "Authorization"
        static let contentType = "Content-Type"
        static let accept = "Accept"
    }
    
    struct Values {
        static func bearer(token: String) -> String {
            return "bearer \(token)"
        }
        static let json = "application/json"
        static let form = "application/x-www-form-urlencoded"
    }
}
