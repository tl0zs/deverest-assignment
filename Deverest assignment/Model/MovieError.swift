//
//  MovieError.swift
//  Deverest assignment
//
//  Created by Zsolt Magyi on 2/27/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import Foundation

struct MovieError: Codable, Error {
    var status_code: Int
    var status_message: String
}
