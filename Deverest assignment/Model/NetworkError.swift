//
//  MovieError.swift
//  Deverest assignment
//
//  Created by Zsolt Magyi on 2/26/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import Foundation


enum NetworkError: Error {
    case requestError
    case noResponseError
    case invalidSessionError
    case invalidJsonFomatError
}
