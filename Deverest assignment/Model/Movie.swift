//
//  Movie.swift
//  Deverest assignment
//
//  Created by Zsolt Magyi on 2/26/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import Foundation
import UIKit

class Movie: Codable {
    var overview: String
    var popularity: Double
    var title: String
    var vote_average: Double
    var adult: Bool
    var original_title: String
    var video: Bool
    var id: Int
    var backdrop_path: String
    var vote_count: Int
    var original_language: String
    
    var poster_path: String
    var budget: Int?
    private var release_date: String
    private var coverImageData: Data?
    
    static func unwrapMovie(movieNode: [String: AnyObject]) -> Movie? {
        let jsonData = try? JSONSerialization.data(withJSONObject: movieNode, options: [])
        do {
            let movie = try JSONDecoder().decode(Movie.self, from: jsonData!)
            return movie
        } catch let error {
            print("Couldn't parse the movie: \(error.localizedDescription)")
            return nil
        }
    }
    
    func getReleaseYear() -> Int? {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-DD"
        guard let date = formatter.date(from: release_date) else { return nil }
        let calendar = Calendar.current

        return calendar.component(.year, from: date)
    }
    
    func getCover() -> UIImage? {
        return coverImageData != nil ? UIImage(data: coverImageData!) : nil
    }
    
    func setCover(data: Data?) {
        coverImageData = data
    }
}
