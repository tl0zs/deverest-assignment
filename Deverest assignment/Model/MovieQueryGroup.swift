//
//  MovieQueryItem.swift
//  Deverest assignment
//
//  Created by Zsolt Magyi on 2/27/20.
//  Copyright © 2020 Zsolt Magyi. All rights reserved.
//

import Foundation

struct MovieQueryItem {
    var key: String
    var value: String
}

enum QueryGroup {
    case discover(page: Int)
    case search(query: String)
    case coverImage
    case budget
    
    func getQueryItems() -> [MovieQueryItem] {
        switch self {
            
        case .discover(let page):
            return [
                MovieQueryItem(key: API.key.rawValue, value: API.value.rawValue),
                MovieQueryItem(key: QueryKeyConstants.language.rawValue, value: QueryValueConstants.english.rawValue),
                MovieQueryItem(key: QueryKeyConstants.sort_by.rawValue, value: QueryValueConstants.popularityDesc.rawValue),
                MovieQueryItem(key: QueryKeyConstants.include_adult.rawValue, value: QueryValueConstants.no.rawValue),
                MovieQueryItem(key: QueryKeyConstants.include_video.rawValue, value: QueryValueConstants.no.rawValue),
                MovieQueryItem(key: QueryKeyConstants.page.rawValue, value: String(page))
            ]
            
        case .search(let query):
            return [
                MovieQueryItem(key: API.key.rawValue, value: API.value.rawValue),
                MovieQueryItem(key: QueryKeyConstants.query.rawValue, value: query)
            ]

        case .budget:
            return [MovieQueryItem(key: API.key.rawValue, value: API.value.rawValue)]
            
        case .coverImage:
            return [MovieQueryItem(key: API.key.rawValue, value: API.value.rawValue)]
        }
    }
}

private enum QueryKeyConstants: String {
    case language
    case sort_by
    case include_adult
    case include_video
    case page
    case query
}

private enum QueryValueConstants: String {
    case english = "en-US"
    case popularityDesc = "popularity.desc"
    case popularityAsc = "popularity.asc"
    case no = "false"
    case yes = "true"
}
